const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const { setupRoutes } = require("./routes");
const { pool } = require("./config/db");

const app = express();

pool
  .connect()
  .then(console.log("successfully connected"))
  .catch((err) => console.error(err))
  .finally(() => pool.end)

const port = process.env.PORT;

app.use(cors({origin: true, credentials: true}));
app.use(morgan("tiny"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static("/public"));

setupRoutes(app);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
