const {
  getAll,
  getById,
  getTasksOf,
  create,
  update,
} = require("../models/tasks.model");

const getTasks = async (req, res) => {
  const tasks = await getAll();
  try {
    res.status(200).json(tasks);
  } catch (error) {
    res.status(500).json({ message: "Error retrieving tasks" });
  }
};

const getUserTasks = async (req, res) => {
    const userid = req.params.id
    const tasks =  await getTasksOf(userid)
    try {
      res.status(200).json(tasks)
    } catch (error) {
      console.error(error);
    }
}

getOneTask = async (req, res) => {
  const task_id = req.params.id;
  const task = await getById(task_id);
  try {
    res.status(200).json(task);
  } catch (error) {
    console.error(err);
    res.status(500).json({ message: "Error retrieving task" });
  }
};

const createTask = async (req, res) => {
  const { task_title, completed, userid } = req.body;
  const task = await create({ task_title, completed, userid })
    .then(
      res.status(201).json({ message: "Task created", task: { ...req.body } })
    )
    .catch((err) => {
      console.error(err);
      res.status(500).json({ message: "Error posting task" });
    });
};

const updateTask = async (req, res) => {
  const task_id = req.params.id;
  const { userid, completed, task_date, task_description } = req.body;
  const updatedTask = await update({
    userid,
    completed,
    task_date,
    task_description,
    task_id,
  });
  try {
    res.status(200).json({ message: "Task updated", updated: { ...req.body } });
  } catch (error) {
    res.status(500).json({ message: "Error updating task" });
  }
};

// const deleteTask = async (req, res) => {
//   const task_id = req.params.id;
//   const deletedTask = await deleteOne(task_id);
//   try {
//     res.status(200).json({ message: "task deleted" });
//   } catch (error) {
//     console.error(err);
//     res.status(500).json({ message: "Error deleting task" });
//   }
// };

module.exports = { getTasks, getOneTask, getUserTasks, createTask, updateTask };
