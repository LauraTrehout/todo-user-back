const {
  getAll,
  getById,
} = require("../models/users.model");

const getUsers = async (req, res) => {
  const users = await getAll();
  try {
    res.status(200).json(users);
  } catch (error) {
    res.status(500).json({ message: "Error retrieving users" });
  }
};

const getOneUser = async (req, res) => {
  const user_id = req.params.id;
  const user = await getById(user_id);
  try {
    res.status(200).json(user);
  } catch (error) {
    console.error(err);
    res.status(500).json({ message: "Error retrieving user" });
  }
};

module.exports = { getUsers, getOneUser };
