const { getByEmail, create } = require("../models/auth.model");
const { hashPassword, verifyPassword } = require("../utils/argon2");
const { createToken } = require("../utils/jwt");
const { validationResult } = require("express-validator");
const jwt = require("jsonwebtoken");

const registerUser = async (req, res) => {
  const { name, email, password } = req.body;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json(errors.array());
  }
  try {
    const user = await getByEmail(email);
    if (user) {
      throw new Error("DUPLICATA");
    }
    const hashedPassword = await hashPassword(password);
    await create({ name, email, hashedPassword });
    return res.status(201).json({ message: "User registered" });
  } catch (error) {
    if (error.message === "DUPLICATA") {
      return res.status(401).json({ message: "User already exists" });
    } else {
      res.status(500).json({ message: "Error registering user" });
    }
  }
};

const logUser = async (req, res) => {
  const { email, password } = req.body;
  try {
    const user = await getByEmail(email);
    if (!user) {
      throw new Error("NOT_FOUND");
    } else {
      const verifiedPassword = await verifyPassword(
        user.hashedpassword,
        password
      );
      if (!verifiedPassword) {
        throw new Error("WRONG_CREDENTIALS");
      } else {
        const access_token = createToken({
          id: user.id,
          name: user.user_name,
          email: email,
        });
        res
          .status(200)
          .header("access_token", access_token)
          .json({ access_token: access_token });
      }
    }
  } catch (error) {
    if (error.message === "NOT_FOUND") {
      res.status(404).json({ message: "User doesn't exist" });
    } else if (error.message === "WRONG_CREDENTIALS") {
      res
        .status(401)
        .json({ message: "Email and password are not corresponding" });
    } else {
      return res.status(500).json({ message: "Error loging user" });
    }
  }
};

const verifyToken = async (req, res) => {
  const { token } = req.body
  if (!token) {
    return res.status(401).json({ message: "No token found" });
  }
  try {
    const verified = await jwt.verify(token, process.env.PRIVATE_KEY);
    return res.status(200).json({ id: verified.id, name: verified.name });
  } catch (error) {
    return res.status(401).json({ message: "Token is not valid" });
  }
};

module.exports = { registerUser, logUser, verifyToken };
