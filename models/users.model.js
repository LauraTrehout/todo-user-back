const { pool } = require("../config/db");

const getAll = () => {
  let sql = `SELECT * FROM users`;
  return new Promise((resolve, reject) => {
    pool.query(sql, (err, result) => {
      if (err) reject(err);
      resolve(result.rows);
    });
  });
};

const getById = (user_id) => {
  let sql = `SELECT * FROM users WHERE id = $1`;
  return new Promise((resolve, reject) => {
    pool.query(sql, [user_id], (err, result) => {
      if (err) reject(err);
      resolve(result.rows[0]);
    });
  });
};

module.exports = { getAll, getById };
