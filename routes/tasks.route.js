const taskRouter = require("express").Router();
const { getTasks, getOneTask, getUserTasks, createTask, updateTask } = require("../controllers/tasks.controller");

taskRouter.get("/", getTasks);
taskRouter.get("/:id", getOneTask);
taskRouter.get("/private/:id", getUserTasks);
taskRouter.post('/', createTask )
taskRouter.put('/:id', updateTask )
// taskRouter.delete('/:id', deleteTask )

module.exports = taskRouter;
