const userRouter = require("express").Router();
 const { getUsers, getOneUser } = require('../controllers/users.controller')

userRouter.get('/', getUsers)
userRouter.get('/:id', getOneUser)

module.exports = userRouter