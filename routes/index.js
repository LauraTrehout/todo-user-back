const taskRouter = require("./tasks.route");
const userRouter = require("./users.route");
const authRouter = require("./auth.route");

const setupRoutes = (app) => {
  app.use("/tasks", taskRouter);
  app.use("/users", userRouter);
  app.use("/auth", authRouter);
};

module.exports = { setupRoutes };
