const authRouter = require("express").Router();
const { registerUser, logUser, verifyToken } =  require('../controllers/auth.controllers')
const { check } = require("express-validator");

authRouter.post('/signup', [check("name", "Name must be at least 2 characters").isLength({min: 2}), check("email", "Email is not valid").isEmail(), check("password", "Password must be at least 8 characters").isLength({ min: 8 })], registerUser)
authRouter.post('/login', logUser)
authRouter.post('/token', verifyToken)

module.exports = authRouter
