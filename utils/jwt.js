const jwt = require("jsonwebtoken");

const createToken = ({ id, name, email }) => {
  return jwt.sign(
    { id: id, name: name, email: email },
    process.env.PRIVATE_KEY,
    {
      expiresIn: "3600s",
    }
  );
};

module.exports = { createToken };
